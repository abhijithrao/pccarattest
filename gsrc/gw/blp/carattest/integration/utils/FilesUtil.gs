package gw.blp.carattest.integration.utils

/**

 * User: Abhijith.Rao1
 * Date: 4/21/16
 * Time: 2:05 PM
 *FilesUtil Contains all the common util functions that are required
 */
uses java.lang. *
uses java.io.File
uses java.io.FileInputStream
uses java.io.FileOutputStream
uses java.io.IOException
uses java.io.InputStream
uses java.io.OutputStream
uses java.nio.channels.FileChannel
uses java.nio.file.Path
uses java.nio.file.Paths
uses org.apache.commons.io.IOUtils
uses org.apache.commons.io.FileUtils
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory

public class FilesUtil {
  private static final var LOGGER: Logger = LoggerFactory.getLogger(FilesUtil)
  public static function touch(final file: File): void {
    if (file.exists()) {
      if (file.isDirectory()) {
        throw new IOException("File \'" + file + "\' exists but is a directory")
      }
      if (file.canWrite() == false) {
        throw new IOException("File \'" + file + "\' cannot be written to")
      }
    } else {
      final var parent: File = file.getParentFile()
      if (parent != null) {
        if (!parent.mkdirs() and !parent.isDirectory()) {
          throw new IOException("Directory \'" + parent + "\' could not be created")
        }
      }
    }
    final var success: boolean = file.setLastModified(System.currentTimeMillis())
    FileUtils.touch(file)
    if (!success) {
      throw new IOException("Unable to set the last modification time for " + file)
    }
  }

  public static function touchFile(final file: File): void {
    FileUtils.touch(file)
  }

  public static function getFileExtensions(fileName: String): String {
    if (fileName != null and fileName.contains(".")) {
      var extension: String = fileName.substring(fileName.indexOf("."))
      return extension
    } else {
      return null
    }
  }

  public static function nioCopyFromSourceToDestination(source: File, dest: File): void {
    var f_in: FileInputStream = null
    var f_out: FileOutputStream = null
    LOGGER.info("Performing force copy of source \'" + source + "\' to \'" + dest + "\'")
    if (!source.exists()) {
      LOGGER.error("File Cannot be copied as source is either already copied to destination / src doesnt exist")
      throw new IOException("Source File missing .... " + source)
    }
    if (!dest.exists()) {

      try {
        f_in = new FileInputStream(source)

        try {
          dest.createNewFile()
          f_out = new FileOutputStream(dest)
          var in_0: FileChannel = f_in.getChannel()
          var out: FileChannel = f_out.getChannel()
          out.transferFrom(in_0, 0, in_0.size())
        }
            finally {
          IOUtils.closeQuietly(f_out)
        }
      }
          finally {
        IOUtils.closeQuietly(f_in)
      }
    } else {
      LOGGER.info("File Cannot be copied as Destination already exits")
    }
    if (!dest.exists()) {
      throw new IOException("Failed to copy file to new location: " + dest)
    }
  }
  public static function streamCopySrcToDest(source : File, dest : File) : void {
    var inStream : InputStream = null
    var outStream : OutputStream = null
    try {
      var afile : File = source
      var bfile : File = dest
      inStream = new FileInputStream(afile)
      outStream = new FileOutputStream(bfile)
      var buffer = new byte[1024]
      var length_0 : int
      while ((inStream.read(buffer)) > 0) {
      outStream.write(buffer, 0, length_0)
    }
      inStream.close()
      outStream.close()
      afile.delete()
      LOGGER.info("File is copied successful!")
    }
        catch (e : IOException) {
          e.printStackTrace()
        }
  }


  public static function nioCopyAndDelete(source: File, dest: File): void {
    var f_in: FileInputStream = null
    var f_out: FileOutputStream = null
    LOGGER.debug("Performing force copy-and-delete of source \'" + source + "\' to \'" + dest + "\'")

    try {
      f_in = new FileInputStream(source)

      try {
        f_out = new FileOutputStream(dest)
        var in_0: FileChannel = f_in.getChannel()
        var out: FileChannel = f_out.getChannel()
        in_0.transferTo(0, source.length(), out)
      }
          finally {
        IOUtils.closeQuietly(f_out)
      }
    }
        finally {
      IOUtils.closeQuietly(f_in)
    }

    if (!dest.exists()) {
      throw new IOException("Failed to copy file to new location: " + dest)
    }
  }

  public static function deleteFile(filePath: Path): void {
    LOGGER.info("File deleted -??" + filePath.toFile().delete())
  }

  public static function verifyCopiedFiles(source: Path, dest: Path): boolean {
    LOGGER.info("verifyCopiedFiles()")
    if (FileUtils.checksumCRC32(source.toFile()) == FileUtils.checksumCRC32(dest.toFile())) {
      LOGGER.info("The Copied file are identical...")
      return true
    } else {
      return false
    }
  }
}
