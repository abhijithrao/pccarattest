package gw.blp.carattest.integration.utils

uses java.util.GregorianCalendar
uses gw.xml.date.XmlDateTime
uses javax.xml.datatype.DatatypeFactory
uses java.util.Date
uses java.util.TimeZone
uses gw.xml.date.XmlDate
uses java.util.Calendar
uses java.util.Locale
uses gw.xml.date.XmlDate
uses gw.xml.date.XmlMonthDay
uses org.apache.http.util.EncodingUtils

/**
 * User: Abhijt=ith
 * Date: 19/04/16
 * Time: 10:29
 *XmlDateTimeUtil functions required for parse the dates during import of xml into GW dates and vice versa.
 */
class XmlDateTimeUtil {

  static function getXmlDateTime(date : java.util.Date) : XmlDateTime {
    if(date == null) { return null }
    var calendar = new GregorianCalendar();
    calendar.setTime(date);
    var df = DatatypeFactory.newInstance();
    var dateTime = df.newXMLGregorianCalendar(calendar);
    return new XmlDateTime(dateTime.toString())
  }

  static function getDateTime(date : XmlDateTime) : Date {

    if (date == null) {
      return null
    }

    return date.toCalendar(TimeZone.Default).Time
  }

  static function getDateTime(date : XmlDate) : Date {

    if (date == null) {
      return null
    }

    return date.toCalendar(TimeZone.Default).Time
  }
  public static function getDiffYears(first : Date, last : Date) : int {
    var a : Calendar = getCalendar(first)
    var b : Calendar = getCalendar(last)
    var diff : int = b.get(Calendar.YEAR) - a.get(Calendar.YEAR)
    if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH) or (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) and a.get(Calendar.DATE) > b.get(Calendar.DATE))) {
      diff--
    }
    return diff
  }
  public static function getCalendar(date : Date) : Calendar {
    var cal : Calendar = Calendar.getInstance(Locale.US)
    cal.setTime(date)
    return cal
  }

  public static function getYears(time : java.util.Date) : int {
    var now_0 : org.joda.time.DateTime = org.joda.time.DateTime.now()
    var then = new org.joda.time.DateTime(time.getTime())
    return org.joda.time.Years.yearsBetween(now_0, then).getYears()
  }

  public static function convertToXmlMonthDay(dateTime: Date): XmlMonthDay {
    var xmlMonthDay = new XmlMonthDay()
    xmlMonthDay.Month = dateTime.MonthOfYear
    xmlMonthDay.Day = dateTime.DayOfMonth
    return xmlMonthDay
  }

  // todo static

  public static function convertToXmlDate(dateTime: Date): XmlDate {
    var xmlDate = new XmlDate()
    xmlDate.Year = dateTime.YearOfDate
    xmlDate.Month = dateTime.MonthOfYear
    xmlDate.Day = dateTime.DayOfMonth
    return xmlDate
  }

  /**
   * Returns Fixed Witdh ASCII Value
   */
  @Param("s", "input string")
  @Param("len", "Field width")
  @Returns("Fixed width ASCII String")
  public static function getFixedWitdhASCIIValue(s: String, len: int): String {
    s = getAscii(s)

    if (s.length <= len) {
      return s
    }

    return s.substring(0, len)
  }

  private static function getAscii(s: String): String {
    return EncodingUtils.getAsciiString(EncodingUtils.getAsciiBytes(s))
  }


}
