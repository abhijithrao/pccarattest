package gw.blp.carattest.integration.utils

uses gw.api.system.PLConfigParameters
uses gw.api.util.DisplayableException
uses gw.blp.carattest.integration.mappers.ExternalClaimToPcClaimAttestationMapper
uses gw.pl.persistence.core.Bundle
uses gw.plugin.util.SequenceUtil
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory
uses java.io.File
uses java.lang.Exception
uses java.util.HashMap
uses java.util.Date
uses gw.api.database.Relop
uses gw.blp.carattest.integration.common.CommonConstants_Ext
uses java.util.Set
uses gw.api.database.DBFunction
uses gw.api.database.DateDiffPart

/**
 *
 * User: Abhijith.Rao1
 * Date: 6/11/16
 * Time: 3:10 PM
 * Contains all the Util functions for creation of Entities
 */
public class CarAttestUtil {

  static var LOGGER: Logger = LoggerFactory.getLogger(CarAttestUtil)
  var externalImportedClaims: List<gw.blp.carattest.integration.xsd.claimmodel.anonymous.elements.ClaimSystem_Claims_Claim>
  private static final var file_name = IntegrationPropertiesUtil.getProperty("ClaimImport_file_name")
  private static final var source_File_path = IntegrationPropertiesUtil.getProperty("ClaimImport_source_File_path")
  private static final var targetFile = IntegrationPropertiesUtil.getProperty("ClaimImport_targetFile")
  private static final var date_time_format = IntegrationPropertiesUtil.getProperty("ClaimImport_date_time_format")
  /**
   *  createCarAttestation()
   *  If any claims found for the policy , then create ClaimAttestation Entity
   */
  @Param("period", "PolicyPeriod reference on which claim Attestation would be created")
  @Param("_externalMatchingClaim", "The Matching Claim from the 3rd party system for the policy with in range of 5 years")
  @Returns("void")
  function createCarAttestation(period: PolicyPeriod, _externalMatchingClaim: List<gw.blp.carattest.integration.xsd.claimmodel.anonymous.elements.ClaimSystem_Claims_Claim>) {
    LOGGER.info("Matching Claims Found in the imported data..Creating claim attestation entities for..() " + period.PolicyNumber)

    var account = period.Policy.Account
    var person = (account.AccountHolderContact as Person)

    LOGGER.info("Creating claim attestation entities for..Adding data to bundle () " + period.PolicyNumber)

    if (_externalMatchingClaim != null && _externalMatchingClaim.Count>0) {

        gw.transaction.Transaction.runWithNewBundle(\newBundle -> {

          var policyClaimAttestation = new PolicyClaimAttestation_Ext(newBundle)
          var mapper = new ExternalClaimToPcClaimAttestationMapper(_externalMatchingClaim, period)
          policyClaimAttestation = mapper.mapPolicyClaimAttestation(policyClaimAttestation)

        }, PLConfigParameters.UnrestrictedUserName.Value)
      LOGGER.debug("=================Bundle Comitted ======================")

   }
    else
    {
       LOGGER.info("No Policy ClaimAttestation Entity Created for :" + period.PolicyNumber)
    }
  }

  /** sampleCreateDocument()
   *  Returns true if the property value identified by property name evaluates to true;
   *  if a property is not boolean, returns false.
   */
  @Param("PolicyPeriod", "PolicyPeriod")
  @Param("_PolicyClaimAttestation1", "The entity for which an attestation to be created")
  @Returns("True if the property value identified by property name evaluates to true")
  function sampleCreateDocument(_period: PolicyPeriod, _PolicyClaimAttestation1: PolicyClaimAttestation_Ext) {
    var _PolicyClaimAttestation: PolicyClaimAttestation_Ext = _PolicyClaimAttestation1
    var period: PolicyPeriod = _period

    _PolicyClaimAttestation.claimAttestations.each(\claimAttestation -> {

      try {

        gw.transaction.Transaction.runWithNewBundle(\bundle -> {

          bundle.add(claimAttestation)

          var document = createAndStoreCarAttestationDocument(period, claimAttestation, _PolicyClaimAttestation, bundle)
        }, PLConfigParameters.UnrestrictedUserName.Value)
      } catch (ex) {
        LOGGER.info("CreateDocument --> Exception logged: ", ex)
        throw new DisplayableException("Cannot Create Document for claim Attestation")
      }
    })
  }

  /*
  * createAndStoreCarAttestationDocument()
   *  This was done on trial, but failed to craete document OOTB as looping mechanism is not supported,
   *  This is unsupported method.
  * */
  private  function createAndStoreCarAttestationDocument(period: PolicyPeriod, _ClaimAttestation: ClaimAttestation_Ext, _PolicyClaimAttestation: PolicyClaimAttestation_Ext, bundle: Bundle): Document {

    final var CLAIM_ATTESTATION_TEMPLATE_DOC = "CarAttestation_"
    final var CLAIM_ATTESTATION_TEMPLATE_EXTENSION = ".gosu.rtf"

    var contextObjects = new HashMap()
    contextObjects.put("PolicyPeriod", period)
    contextObjects.put("ClaimAttestation", _ClaimAttestation)
    contextObjects.put("PolicyClaimAttestation", _PolicyClaimAttestation)


    var document = new Document(_ClaimAttestation)
    bundle.add(document)
    var nextInt = SequenceUtil.getSequenceUtil().next(100, 1)   //     todo
    document.Policy = period.Policy
    document.ClaimAttestation = _ClaimAttestation
    document.Name = "CarAttestation" + nextInt + ".rtf"
    document.Type = typekey.DocumentType.TC_CLAIM_ATTESTATION
    document.MimeType = "application/rtf"

    document.ClaimAttestation = _ClaimAttestation


    var plugin = gw.plugin.Plugins.get(gw.plugin.document.IDocumentTemplateSource)
    var locale = gw.api.util.LocaleUtil.getDefaultLocale()
    var language = period.Policy.Account.PrimaryLanguage
    var docName = CLAIM_ATTESTATION_TEMPLATE_DOC + language + CLAIM_ATTESTATION_TEMPLATE_EXTENSION
    LOGGER.debug("Language is ${language} - DocName ${docName}")
    var descriptor = plugin.getDocumentTemplate(docName, locale)
    if (descriptor != null) {
      try {
        gw.document.DocumentProduction.createAndStoreDocumentSynchronously(descriptor, contextObjects, document)
      } catch (ex) {
        LOGGER.debug("CreateDocument --> There was a problem while trying to create and store the document")
        throw new Exception("Exception encountered trying to add document with doc UID: ${document.DocUID}\n", ex)     //todo check tis
      }
    } else {
      LOGGER.warn("CreateDocument --> Descriptor is null for template file: ${CLAIM_ATTESTATION_TEMPLATE_DOC} and locale = ${locale}")
    }

    return document
  }

  /**   processSourceFileAndGetClaims() : - Imports the Claim import file from a common location and parses to its object
  *  which gives us pojo list containing Claim Data
   */
  @Param("fileName", "String")
  @Param("sourceFilePath", "Source file path string")
  @Returns("The ClaimSystem_Claims_Claim after parsing the xml")
  function processSourceFileAndGetClaims(fileName: String, sourceFilePath: String): List<gw.blp.carattest.integration.xsd.claimmodel.anonymous.elements.ClaimSystem_Claims_Claim> {
    LOGGER.info("ClaimImport : processSourceFileAndGetClaims : Begins ")
    var file: File = null
    try {
      LOGGER.info("ClaimImport : fileName : ${fileName} : sourceFilePath :${sourceFilePath}")
      var sourcePath ="${sourceFilePath}${fileName}"

      LOGGER.debug("ClaimImport : sourcePath : ${sourcePath}")

      file = new File(sourcePath)
      if (!file.canRead()) {
        LOGGER.error("Import File cannot be read for some reason, so not proceeding with the processing..!")
      }else{
        LOGGER.info(" ClaimImport file can  be read")
        var importedData = gw.blp.carattest.integration.xsd.claimmodel.ClaimSystem.parse(file)
        if (importedData != null && importedData.Claims != null && importedData.Claims.Claim.size() > 0){
          LOGGER.info("Importing external claims......!!")
          externalImportedClaims = importedData.Claims.Claim
          return externalImportedClaims
        }
      }
    }
    catch (ex: Exception) {
      handleClaimImportException(ex, "Exception during file read and import : ${file.Name}")

    }

    return null
  }

  /**
   * Increments Operations failed and log exception cause and stack trace
   */
  @Param("ex", "The data type is: Exception")
  @Param("message", "The data type is: String")
  function handleClaimImportException(ex: Exception, message: String) {
    LOGGER.error(message)
    LOGGER.error(ex.StackTraceAsString)
  }


  /**   findClaimsAndCreateAttestations() : - This method finds if any matching 5 claims for the policynumber for last 5 year
   *     Method load file frm remote location and copy it to local location
   *     and updated file content to database
   */
  @Param("PolicyPeriod", "PolicyPeriod")
  @Param("_externalImportedClaims", "Imported claim list")
  @Returns("The ClaimSystem_Claims_Claim of filtered and matching criteria claims")
  function findClaimsAndCreateAttestations(period: PolicyPeriod,
                                           _externalImportedClaims: List<gw.blp.carattest.integration.xsd.claimmodel.anonymous.elements.ClaimSystem_Claims_Claim>)
      : List<gw.blp.carattest.integration.xsd.claimmodel.anonymous.elements.ClaimSystem_Claims_Claim> {

    var polNumber = period.PolicyNumber
    var matchingClaimDataWithAllCriteria = new List<gw.blp.carattest.integration.xsd.claimmodel.anonymous.elements.ClaimSystem_Claims_Claim>()

   //The import file might contain more than  5 years claim but we are interested only last five years claim and hence we are filtering the same below.
    var matchingClaimsData = _externalImportedClaims.where(\importedClaim ->
          importedClaim.ClaimInfo.PolicyNumber == polNumber &&
          XmlDateTimeUtil.getYears(XmlDateTimeUtil.getDateTime(importedClaim.ClaimInfo.CreationDate)) > CommonConstants_Ext.NUMBER_OF_YEARS_FROM_TODAY )

    //There can be two kind of vehicles either coming personal or commercial , in order to check whether the vehicle exists on the policy ,we need to check from the
    // policy lines of the policy , and hence there are two checks below in order to validate to match the Vin from the importe claim./
    matchingClaimsData.each(\claim -> {
      var mapperObj = new ExternalClaimToPcClaimAttestationMapper(matchingClaimsData, period)

      if (period.PersonalAutoLineExists){
        var vehicle = mapperObj.personalAutoLineVehicles.where(\vehicle -> vehicle.Vin.equalsIgnoreCase(claim.ClaimInfo.PlateNumber))
        if ( vehicle != null){
          matchingClaimDataWithAllCriteria.add(claim)
        }
      }
      if (period.BusinessAutoLineExists){
        var vehicle = mapperObj.businessAutoLineVehicles.where(\vehicle -> vehicle.Vin.equalsIgnoreCase(claim.ClaimInfo.PlateNumber))
        if (vehicle != null){
          matchingClaimDataWithAllCriteria.add(claim)
        }
      }
    })

    if (matchingClaimDataWithAllCriteria.size() > 0){
      LOGGER.debug("Matching claims found for policy ${matchingClaimDataWithAllCriteria}")
      LOGGER.debug("Matching claims found count ${matchingClaimDataWithAllCriteria.Count}")
      return matchingClaimDataWithAllCriteria
    }
    return null
  }

  /**   createClaimAttestationXMLForPolicy() - This method creates the ClaimAttestations for the given policy from Screen on Demand
   *  The policyHolder can request for the car attestations, then we dynamicallt check
   *  whether a claim is imported
   *  if matching claims found for the given policy and Vehicle plate number for last 5 years
   *  also "validateIfClaimAttestationGenerated" condition is checked to verify no claim attestation is craeted on current date
   *  to eliminate any duplicates,
   */
  @Param("PolicyPeriod", "PolicyPeriod")
   public function createClaimAttestationXMLForPolicy(_policyPeriod: PolicyPeriod) {
    LOGGER.info("createClaimAttestationXMLForPolicy(): Begins ")
    try {
      externalImportedClaims = processSourceFileAndGetClaims(file_name, source_File_path)
      if (externalImportedClaims.Count > 0 && _policyPeriod != null && !validateIfClaimAttestationGenerated(_policyPeriod)) {
        var policyPeriod = _policyPeriod
        var matchedClms = findClaimsAndCreateAttestations(policyPeriod, externalImportedClaims)
        if (matchedClms != null) {
          createCarAttestation(policyPeriod, matchedClms)
          //this is creation of entities
          // carAttestationUtil.createCarAttestationXML(policyPeriod)

          var datassurXMLMapAndCreate = new gw.blp.carattest.integration.mappers.PolicyClaimAttestationToDatassurMapper(policyPeriod)

          //creation of Entity to Datassur XML
          datassurXMLMapAndCreate.createDatassurAttestation()

          //TODO to be removed    :- This is not being done, we are not creating the documents , as in OOTB for a custom entity document cannot be created
          //TODO                     and also we cannot apply iteration logic in the custom entity during document generation
          // and also not sure how do we pass the array entity and loop in to display the values in the generated document
          // var matchedClaimAttestationsToBeGeneratedQuery = gw.api.database.Query.make(PolicyClaimAttestation).select().first()
          //sampleCreateDocument(policyPeriod, matchedClaimAttestationsToBeGeneratedQuery)
        }
        else {
          LOGGER.info("createClaimAttestationXMLForPolicy() : No Matching Claim Found for Policy..! ")
        }
      } else {
        LOGGER.info("createClaimAttestationXMLForPolicy() No Claims Imported or Claim attestation already generated for today...! ")
      }
    } catch (e: Exception) {
      LOGGER.error(" Exception in createClaimAttestationXMLForPolicy() method.. : ${e.Message}" )
     e.printStackTrace()
    }
    LOGGER.info("createClaimAttestationXMLForPolicy() : Ends ")
  }

  /** getPoliciesExpiringInDays() - This method returns the list of all policies that are expiring eaxctly 15 days from Today
   * and returns the Policy Period list
   *
   * 2. Condition to apply is , if a policy was explicitly marked for cancellation
   */
  @Param("PolicyPeriod", "PolicyPeriod")
  @Param("_externalImportedClaims", "Imported claim list")
  @Returns("boolean")
  public function getPoliciesExpiringInFifteenDaysAndCancelledYesterday(days :int):List<entity.PolicyPeriod>{
    LOGGER.info("getPoliciesExpiringInFifteenDaysAndCancelledYesterday()  :  Entered the Method")
    var today = Date.CurrentDate

    var policiesAboutToExpireQuery = gw.api.database.Query.make(entity.PolicyPeriod)
    var policiesAboutToExpire = policiesAboutToExpireQuery
        .compare(DBFunction.DateFromTimestamp(policiesAboutToExpireQuery.getColumnRef("PeriodEnd")), Relop.Equals,Date.CurrentDate.addDays(days).trimToMidnight())
        .compare(DBFunction.DateDiff(DateDiffPart.DAYS,policiesAboutToExpireQuery.getColumnRef("PeriodStart"),policiesAboutToExpireQuery.getColumnRef("PeriodEnd")),Relop.GreaterThanOrEquals,days)
        .compare(DBFunction.DateFromTimestamp(policiesAboutToExpireQuery.getColumnRef("PeriodStart")),Relop.LessThanOrEquals, Date.CurrentDate.trimToMidnight())
        .select()

    var policiesExpiredList = policiesAboutToExpire.toList()


    var policiesMarkedForCancellationYesterday = getCancelledPoliciesAsPerYesterday()


     var policiesToBeConsideredForGeneration : Set<entity.PolicyPeriod>

      if(policiesMarkedForCancellationYesterday != null && policiesExpiredList != null){
          policiesToBeConsideredForGeneration = policiesMarkedForCancellationYesterday.union(policiesExpiredList).toSet()
      }else if(policiesMarkedForCancellationYesterday != null){
          policiesToBeConsideredForGeneration = policiesMarkedForCancellationYesterday.toSet()
      }else if(policiesExpiredList != null){
          policiesToBeConsideredForGeneration = policiesExpiredList.toSet()
      }
    LOGGER.info("getPoliciesExpiringInFifteenDaysAndCancelledYesterday()  :  Leaving the Method")
    return policiesToBeConsideredForGeneration?.toList()
  }

  /**   validateIfClaimAttestationGenerated() - This method validates whether there was attestation generated
  * and returns the boolean result ,This is in order to avoid the duplicates , if any ClaimAttestation is
  * generated then we dont pick the policy period for processing.
  *
  * This condition both applies from The Batch and Also from on demand request for claim attestation from the UI
  */
  @Param("PolicyPeriod", "PolicyPeriod")
  @Param("_externalImportedClaims", "Imported claim list")
  @Returns("boolean")
  public function validateIfClaimAttestationGenerated(period :PolicyPeriod) : boolean{
    var today = Date.Today
    var policyClaimAttestationQuery = gw.api.database.Query.make(PolicyClaimAttestation_Ext)
    policyClaimAttestationQuery.and(\ andCriteria -> {

        policyClaimAttestationQuery.compare(PolicyClaimAttestation_Ext#PolicyNumber,Relop.Equals,period.PolicyNumber)
        policyClaimAttestationQuery.compare(PolicyClaimAttestation_Ext#AttestationGenerated, Relop.Equals, true)
        policyClaimAttestationQuery.compare(PolicyClaimAttestation_Ext#CreateTime, Relop.GreaterThanOrEquals, today)
    })

     var policyClaimAttestation = policyClaimAttestationQuery.select()
    return ((policyClaimAttestation != null and policyClaimAttestation.Count>0 ) ? true : false)
  }


  /**
   * getCancelledPoliciesAsPerYesterday() -
   * This method Collates all policyPeriods which are 3 scenrios  1. Past Dated Cancellation 2. Scheduled Cancellation but effective only from today.
   * 3. Current Cancellation and Bound.
   * More Description from Below Two Methods :
   * The Result / List returned is an UNION and Unique Result set of below two methods :
   *
   *   getPoliciesMarkedForCancellationAndJobClosedYesterday()
   *   getPoliciesMarkedForCancellationWithEditEffectiveDateAsOfYesterday()
   *
   *
   *  This Method Basically returns all the Explicitly cancelled Policies Yesterday + Yesterday's Job closed Past Cancellation PolicyPeriods
   */
  @Returns("List<PolicyPeriod>")
  private function getCancelledPoliciesAsPerYesterday() : List<PolicyPeriod>{

    LOGGER.info("getCancelledPoliciesAsPerYesterday()  :  Entered the Method")

    var yesterday = Date.CurrentDate.addDays(-1) // As Batch runs at 1 am, we collate all Cancelled Policies as per yesterday

    var cancelledPolicyCollatedList  : java.util.Set<entity.PolicyPeriod>         //creating a set , if in case of duplicate policyperiods gets picked by any chance
    // Both the query makes sure to return unique policies , to be on the safe unique sets of Period are taken

    var policiesMarkedForCancellationAndJobClosedYesterday              = getPoliciesMarkedForCancellationAndJobClosedYesterday(yesterday)
    var policiesMarkedForCancellationWithEditEffectiveDateAsOfYesterday = getPoliciesMarkedForCancellationWithEditEffectiveDateAsOfYesterday(yesterday)

    // We would be adding the found policy period's List into the set just to maintain its unique / non duplicates.

    if(policiesMarkedForCancellationAndJobClosedYesterday !=null && policiesMarkedForCancellationAndJobClosedYesterday.HasElements &&
        policiesMarkedForCancellationWithEditEffectiveDateAsOfYesterday !=null && policiesMarkedForCancellationWithEditEffectiveDateAsOfYesterday.HasElements){

          cancelledPolicyCollatedList = policiesMarkedForCancellationAndJobClosedYesterday.union(policiesMarkedForCancellationWithEditEffectiveDateAsOfYesterday).toSet()

    }else if(policiesMarkedForCancellationAndJobClosedYesterday !=null && policiesMarkedForCancellationAndJobClosedYesterday.HasElements ){

          cancelledPolicyCollatedList = policiesMarkedForCancellationAndJobClosedYesterday.toSet()
    }
    else if(policiesMarkedForCancellationWithEditEffectiveDateAsOfYesterday !=null && policiesMarkedForCancellationWithEditEffectiveDateAsOfYesterday.HasElements){

          cancelledPolicyCollatedList = policiesMarkedForCancellationWithEditEffectiveDateAsOfYesterday.toSet()

    }
    LOGGER.info("getCancelledPoliciesAsPerYesterday()  :  Leaving the Method")
    return cancelledPolicyCollatedList?.toList()
  }


  /**
   * getPoliciesMarkedForCancellationAndJobClosedYesterday() -
   * This method Collates all policyPeriods which  are :
   * Condition 1 . Cancelled & Bound
   * Condition 2 . Cancelled Yesterday
   *     (Cancel Now Option ) - Which is current Cancellation  - The conditions are such are
   *      2.1 Job will have Closed Date as Of yesterday ( we consider all such jobs which is closed after 12.00 am )
   * Condition 3. Past Dated Cancellation
   *         3.1 For Past Dated Cancellation The Cancellation Date OR EditEffective Date will be before Yesterday
   *
   *  This Method Basically returns all the Explicitly cancelled Policies Yesterday + Yesterday's Job closed Past Cancellation PolicyPeriods
   */
  @Param("yesterday", "Date")
  @Returns("List<PolicyPeriod>")
  private function getPoliciesMarkedForCancellationAndJobClosedYesterday( yesterday : Date ) : List<PolicyPeriod>{
    LOGGER.info("Entered the Method : getPoliciesMarkedForCancellationAndJobClosedYesterday()")

    var cancelledPolicies : List<PolicyPeriod> = new List<PolicyPeriod>()
    var policiesMarkedForCancelAndJobClosedYesterdayQuery = gw.api.database.Query.make(entity.PolicyPeriod)
        .compare("Status",Relop.Equals,PolicyPeriodStatus.TC_BOUND)
    policiesMarkedForCancelAndJobClosedYesterdayQuery.join("Job").cast(Cancellation).compare(Job#CloseDate,Relop.GreaterThanOrEquals,yesterday.trimToMidnight()).compare(Job#CloseDate,Relop.LessThanOrEquals,yesterday.addDays(1).trimToMidnight())
    var policiesMarkedForCancelAndJobClosedYesterday = policiesMarkedForCancelAndJobClosedYesterdayQuery.select().where( \ policy -> policy.EditEffectiveDate.differenceInDays(yesterday) >=0 )
    cancelledPolicies = policiesMarkedForCancelAndJobClosedYesterday?.toList()

    LOGGER.info("Leaving the Method :getPoliciesMarkedForCancellationAndJobClosedYesterday() ")
    return cancelledPolicies

  }

  /**
   * getPoliciesMarkedForCancellationWithEditEffectiveDateAsOfYesterday() - Future Dated Cancellation , but picked up only during when it becomes effective
   * This method Collates all policyPeriods which  are :
   * Condition 1 . Cancelled & Bound
   * Condition 2 . Cancellation was Effective only From Yesterday , but was scheduled and marked for cancellation before Yesterday
   *
   *     (ScheduledCancel option ) -  The conditions are such are
   *      2.1 Job will not have Closed Date as Of yesterday ( we consider all such jobs which is closed before yesterday )
   * Condition 3. EditEffectiveDate of the Cancellation will be on Yesterday's Date
   *           *
   *  This Method Basically returns all the Marked Scheduled cancellation Policies which was effective only from Yesterday's Date,
   *  And its Job Close date would be any time before yesterday.
   */
  @Param("yesterday", "Date")
  @Returns("List<PolicyPeriod>")
  private function getPoliciesMarkedForCancellationWithEditEffectiveDateAsOfYesterday( _yesterday : Date ) : List<PolicyPeriod> {
    LOGGER.info("Entered the Method : getPoliciesMarkedForCancellationWithEditEffectiveDateAsOfYesterday()")

    var cancelledPolicies : List<PolicyPeriod> = new List<PolicyPeriod>()
    var policiesMarkedForCancellationWithEditEffectiveDateAsOfYesterdayQuery = gw.api.database.Query.make(entity.PolicyPeriod)
        .compare("Status",Relop.Equals,PolicyPeriodStatus.TC_BOUND)
        .compare(PolicyPeriod#EditEffectiveDate,Relop.Equals,_yesterday.trimToMidnight().addMinutes(1))
    policiesMarkedForCancellationWithEditEffectiveDateAsOfYesterdayQuery.join("Job").cast(Cancellation).compare(Job#CloseDate,Relop.LessThan,_yesterday.trimToMidnight())
    cancelledPolicies = policiesMarkedForCancellationWithEditEffectiveDateAsOfYesterdayQuery.select()?.toList()

    LOGGER.info("Leaving  the Method : getPoliciesMarkedForCancellationWithEditEffectiveDateAsOfYesterday()")

    return cancelledPolicies
  }
}

