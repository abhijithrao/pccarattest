package gw.blp.carattest.integration.utils

uses com.gw.suite.utilitybelt.properties.PropertiesUtil


/**
 * User: Abhijith.Rao1
 * Date: 4/21/16
 * Time: 12:54 PM
 Integration Common Util for accessing properties file, uses GW utility belt
 */
class IntegrationPropertiesUtil {

  static final var _propertiesFile = "props/integration.properties"

  /**
   *  Properties file
   */
  static property get PROPERTIES_FILE(): String {
    return _propertiesFile
  }

  private construct() {
  }

  /**
   *  Returns a property value by name.
   */
  @Param("propName", "Property Name")
  @Returns("A string value of a property value")
  static function getProperty(propName: String): String {
    return PropertiesUtil.getProperty(PROPERTIES_FILE, propName)
  }

  /**
   *  Returns true if the property value identified by property name evaluates to true;
   *  if a property is not boolean, returns false.
   */
  @Param("propName", "Property Name")
  @Returns("True if the property value identified by property name evaluates to true")
  static function isPropertyOn(propName: String): boolean {
    return Boolean.parseBoolean(getProperty(propName))
  }
}