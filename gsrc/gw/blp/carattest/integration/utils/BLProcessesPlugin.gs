package gw.blp.carattest.integration.utils

uses gw.plugin.processing.ProcessesPlugin
uses gw.plugin.InitializablePlugin
uses gw.plugin.processing.IProcessesPlugin
uses java.util.Map
uses gw.processes.BatchProcess
uses gw.blp.carattest.integration.batch.CreateClaimAttestationBatch_Ext

/**

 * User: Abhijith.Rao1
 * Date: 6/14/16
 * Time: 11:43 AM
 * This is the process plugin which is registered in IProcessesPlugin , we are extending to include the ootb batches in the same so as to not disturb the ootb class.
 *
 */
@Export
class BLProcessesPlugin extends ProcessesPlugin implements InitializablePlugin, IProcessesPlugin {
  private var _pParamMap: Map<Object, Object>
  construct() {
  }

  override function createBatchProcess(type: BatchProcessType, arguments: Object[]): BatchProcess {
    switch (type) {

      case BatchProcessType.TC_CARATTESTATION_EXT:
          return new CreateClaimAttestationBatch_Ext()

       default:
        return super.createBatchProcess(type, arguments)
    }
  }

  override function setParameters(p0: Map<Object, Object>) {
    _pParamMap = p0
  }
}
