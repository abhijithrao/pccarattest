package gw.blp.carattest.integration.mappers

uses java.util.Date
uses gw.blp.carattest.integration.utils.XmlDateTimeUtil
uses java.util.ArrayList
uses java.util.HashMap
uses gw.blp.carattest.integration.common.CommonConstants_Ext
uses java.lang.Exception
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory

/**
 *ExternalClaimToPcClaimAttestationMapper : Maps the externally imported claim and creates Entities which will be used for creation of XML later on.
 * And also as per the business requirement , Every time a new ClaimAttestationn is requested we craete new entity.

 * User: Abhijith.Rao1
 * Date: 6/11/16
 * Time: 3:10 PM
 * The  class Maps the external Claim Values to The Entity ,Uses Builder Pattern to map the sub object one by one
 */
class ExternalClaimToPcClaimAttestationMapper {


  private static var LOGGER: Logger = LoggerFactory.getLogger(ExternalClaimToPcClaimAttestationMapper)
  public var externalMatchingClaim: List<gw.blp.carattest.integration.xsd.claimmodel.anonymous.elements.ClaimSystem_Claims_Claim>
  public var businessAutoLineVehicles: List<BusinessVehicle> = new ArrayList<BusinessVehicle>()
  public var personalAutoLineVehicles: List<PersonalVehicle> = new ArrayList<PersonalVehicle>()
  public var isPersonalAutoLine: boolean = false
  public var isBusinessAutoLine: boolean = false
  public var period: PolicyPeriod

  construct(_externalMatchingClaim: List<gw.blp.carattest.integration.xsd.claimmodel.anonymous.elements.ClaimSystem_Claims_Claim>, _period: PolicyPeriod) {
    externalMatchingClaim = _externalMatchingClaim
    period = _period
    isPersonalAutoLine = period.PersonalAutoLineExists
    isBusinessAutoLine = period.BusinessAutoLineExists
    findMatchingVehicleNumberInPolicyLine()
  }

  /**
   * function mapPolicyClaimAttestation()- Populates a root PolicyClaimAttestation_Ext for each Policy
   * @Param PolicyClaimAttestation_Ext each Policy whose ClaimAttestation Entity is to be created
   * @return   PolicyClaimAttestation_Ext populates the child entities and returns it
   */
  function mapPolicyClaimAttestation(_policyClaimAttestation: PolicyClaimAttestation_Ext): PolicyClaimAttestation_Ext {

    try {

        _policyClaimAttestation.JobNumber = period.Job.JobNumber
        _policyClaimAttestation.PolicyNumber = period.PolicyNumber

        externalMatchingClaim.each(\claim -> {

          var claimAttestation : ClaimAttestation_Ext = mapClaimAttestations(claim)
          if(claimAttestation != null ){ // We do not want claims which has incidents falling out of the range and explicitly assigned null
            // from mapClaimAttestations(claim) , which ensurers the statement is skipped and null is returned at the end.
            // Only the valid claimAttestations are created
            _policyClaimAttestation.addToClaimAttestations(claimAttestation)
          }
        })
        if(_policyClaimAttestation?.claimAttestations?.length>0){
            return _policyClaimAttestation
        }
     }
     catch (e: Exception) {
        LOGGER.error("Exception in mapPolicyClaimAttestation: ${e.StackTraceAsString}")
    }
    return null
  }

  /** Builder Pattern
   * function mapClaimAttestations()- Populates a ClaimAttestation_Ext for each Claims found for the policy
   * @Param ClaimAttestation_Ext each Claim
   * @return   ClaimAttestation_Ext populated entities and returns it
   */
  function mapClaimAttestations(claim: gw.blp.carattest.integration.xsd.claimmodel.anonymous.elements.ClaimSystem_Claims_Claim): ClaimAttestation_Ext {
    var claimAttestation = new ClaimAttestation_Ext()

    claimAttestation.AttestationInformation = mapAttestationInformation(claim)
    claimAttestation.InsuranceTaker         = mapInsuranceTaker()
    claimAttestation.CompanyNBBCode         = ScriptParameters.CarattestAttestationCompanyNBBCode

    var histories : DisasterHistory_Ext[] = mapDisasterHistory(claim)
    if(histories==null){
        return null //If not even one incident found, no point creating claim Attestation entity
    }
    claimAttestation.DisasterHistory = histories

    return claimAttestation
  }

  /** Builder Pattern
   * function mapAttestationInformation()- Populates a AttestationInformation_Ext for each Claims found for the policy
   * @Param each Claim
   * @return   AttestationInformation_Ext populated entities and returns it
   */
  function mapAttestationInformation(claim: gw.blp.carattest.integration.xsd.claimmodel.anonymous.elements.ClaimSystem_Claims_Claim): AttestationInformation_Ext {

    var attestationInformation = new AttestationInformation_Ext()

    var map = getBodyTypeCodeAndVehicleUse(claim.ClaimInfo.PlateNumber)
    attestationInformation.PolicyNumber       = period.PolicyNumber
    attestationInformation.PlateNumber        = claim.ClaimInfo.PlateNumber
    attestationInformation.ContrStartingDate  = period.PeriodStart
    attestationInformation.ContrExpiryDate    = period.PeriodEnd
    attestationInformation.ContrEndingDate    = period.Canceled ? period.EditEffectiveDate : period.PeriodEnd
    attestationInformation.VehicleType        = map.get(CommonConstants_Ext.VEHICLE_BODY_TYPECODE).toString()
    attestationInformation.VehicleUse         = map.get(CommonConstants_Ext.VEHICLE_USE).toString()
    attestationInformation.CreationDate       = XmlDateTimeUtil.getDateTime(claim.ClaimInfo.CreationDate)

    return attestationInformation
  }

  /** Builder Pattern
   * function mapInsuranceTaker()- Populates a InsuranceTaker_Ext for each Claims found for the policy
   * @Param each Claim
   * @return   InsuranceTaker_Ext populated entities and returns it
   */
  function mapInsuranceTaker() : InsuranceTaker_Ext {
    var account = period.Policy.Account
    var person = (period.Policy.Account.AccountHolderContact as Person)
    var insuranceTaker = new InsuranceTaker_Ext()

    insuranceTaker.Language   = mapLanguageCode(account.PrimaryLanguage)
    insuranceTaker.Gender     = mapGenderCode(person.Gender.Code)
    insuranceTaker.LastName   = person.LastName
    insuranceTaker.FirstName1 = person.FirstName
    insuranceTaker.birthDate  = person.DateOfBirth
    //TODO: Parse to house number and box code //No Requiment to map this yet , hence leaving it in scope for fututure mapping
    insuranceTaker.Street     = person.PrimaryAddress.AddressLine1
    insuranceTaker.HouseNumber= person.PrimaryAddress.AddressLine3
    insuranceTaker.ZIPCode = person.PrimaryAddress.PostalCode
    // TODO: verifiy if that's correct field
    insuranceTaker.City       = person.PrimaryAddress.City
    insuranceTaker.CountryCode= person.PrimaryAddress.Country.Code
    // TODO: firstname2, firstname3 and birthplace

    return insuranceTaker
  }

  /** Builder Pattern
   * function mapDisasterHistory()- Populates a DisasterHistory_Ext array for each Claims found for the policy
   * @Param each Claim
   * @return   DisasterHistory_Ext populated entities and returns it
   */
  function mapDisasterHistory(claim : gw.blp.carattest.integration.xsd.claimmodel.anonymous.elements.ClaimSystem_Claims_Claim) : DisasterHistory_Ext[] {
    var disasterHistories = new List<DisasterHistory_Ext>()

    claim.Incidents.Incident.each(\incident -> {
      //Double checking the incident not to be populated which falls out of range ibetween start date and End date of the policy

      if(period.EditEffectiveDate.differenceInDays(XmlDateTimeUtil.getDateTime(incident.DisasterDate))>=0 && period.PeriodEnd.differenceInDays(XmlDateTimeUtil.getDateTime(incident.DisasterDate))<=0 )     {

          var disasterHistory = new DisasterHistory_Ext()

          disasterHistory.DisasterDate    = XmlDateTimeUtil.getDateTime(incident.DisasterDate)
          disasterHistory.DisasterType    = incident.DisasterType
          disasterHistory.DriverLastName  = incident.Driver.LastName
          disasterHistory.DriverFirstName = incident.Driver.FirstName
          disasterHistory.DriverBirthDate = XmlDateTimeUtil.getDateTime(incident.Driver?.BirthDate)
          disasterHistory.AmountPaidArt29b      = incident.AmountPaidArt29b
          disasterHistory.AmountOfCompensation  = incident.AmountOfCompensation
          disasterHistory.Closed                = incident.Closed
          disasterHistory.DriverResponsibility  = incident.DriverResponsability

          disasterHistories.add(disasterHistory)
     }
    })
    return disasterHistories
  }
  /**
   * function mapGenderCode()- Returns the Gender equivalent code needed for Datassur
   * @Param GenderType
   * @return  String Code
   */
  static function mapGenderCode(gender : GenderType) : String {
    switch (gender) {
      case(GenderType.TC_F.Code): return "1"
      case(GenderType.TC_M.Code): return "2"
        default: return "3"
    }
  }
  /**
   * function mapLanguageCode()- Returns the LanguageCode equivalent code needed for Datassur
   * @Param LanguageType
   * @return  String Code
   */
  static function mapLanguageCode(language : LanguageType) : String {
    // TODO: Add other supported language types
    switch (language) {
      case(LanguageType.TC_EN_US): return "4"
        default: return "0"
    }
  }

  /**
   * function findMatchingVehicleNumberInPolicyLine()-
   *
   * Called from constructor, populates the global variables Vehicles in order to avoid checking multiple times the type of PolicyLine, This helps to get the matching
   * plate numbers in the claim imported list. And to retrieve the corresponding matching claims.
   */
  private function findMatchingVehicleNumberInPolicyLine() {
    if (isPersonalAutoLine) {

      period.PersonalAutoLine.Vehicles.each(\elt -> {

        LOGGER.info(elt.Vin)   LOGGER.info(elt.VehicleUse)    LOGGER.info("CarAttestCode" + elt.VehicleUse.CarAttestCode)
        LOGGER.info(elt.VehicleType)LOGGER.info(elt.VehicleType.Code)LOGGER.info(elt.BodyType) LOGGER.info(elt.BodyType.Code)

        personalAutoLineVehicles.add(elt)
      })
    }
    else if (isBusinessAutoLine){
      period.BusinessAutoLine.Vehicles.each(\elt -> {

        LOGGER.info(elt.Vin)  LOGGER.info(elt.VehicleUse)   LOGGER.info("CarAttestCode" + elt.VehicleUse.CarAttestCode)
        LOGGER.info(elt.VehicleType) LOGGER.info(elt.VehicleType.Code)LOGGER.info(elt.BodyType) LOGGER.info(elt.BodyType.Code)

        businessAutoLineVehicles.add(elt)
      })
    }
  }
  /**
   * function getBodyTypeCodeAndVehicleUse()-
   * @Param plateNumber
   * This is generic function which determines the type of Vehicle type depending on the lines and returns the Map containing the carattestation vehicle use code
   *
   */
  private function getBodyTypeCodeAndVehicleUse(plateNumber : String) : HashMap<String, String>
  {
    var localMap = new HashMap<String, String>()

    if (isPersonalAutoLine) {

      var vehicle = personalAutoLineVehicles.firstWhere(\elt -> elt.Vin.equalsIgnoreCase(plateNumber))
      LOGGER.debug("addding vehicle.VehicleUse.CarAttestCode" + vehicle.VehicleUse.CarAttestCode)
      LOGGER.debug("addding vehicle.body" + vehicle.BodyType.Code)
      localMap.put(CommonConstants_Ext.VEHICLE_BODY_TYPECODE, vehicle.BodyType.Code)
      localMap.put(CommonConstants_Ext.VEHICLE_USE, vehicle.VehicleUse.CarAttestCode)
      return localMap
    }
    else if (isBusinessAutoLine){

      var vehicle = businessAutoLineVehicles.firstWhere(\elt -> elt.Vin.equalsIgnoreCase(plateNumber))
      localMap.put(CommonConstants_Ext.VEHICLE_BODY_TYPECODE, vehicle.BodyType.Code)
      localMap.put(CommonConstants_Ext.VEHICLE_USE, vehicle.VehicleUse.CarAttestCode)
      return localMap
    }

    return null
  }

  /**
   * handleMappingException
   */
  @Param("ex", "The data type is: Exception")
  @Param("message", "The data type is: String")
  function handleMappingException(ex: Exception, message: String) {
    LOGGER.error(message)
    LOGGER.error(ex.StackTraceAsString)
  }

  /**
   * mapDriverResponsibility()
   * The fault rating for the driver fault, not used for now, as business scope
   */
  private function mapDriverResponsibility(fault: String): String {
    switch (fault) {
      case(FaultRating.TC_1.DisplayName): return "1"
      case(FaultRating.TC_2.DisplayName): return "2"
      case(FaultRating.TC_3.DisplayName): return "3"
      case(FaultRating.TC_4.DisplayName): return "4"

        default: return "3"
    }
  }

}