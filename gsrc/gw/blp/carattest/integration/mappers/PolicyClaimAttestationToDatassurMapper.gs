package gw.blp.carattest.integration.mappers

uses gw.api.database.Relop
uses gw.api.system.PLConfigParameters
uses gw.api.system.database.SequenceUtil
uses gw.blp.carattest.integration.utils.IntegrationPropertiesUtil
uses gw.blp.carattest.integration.xsd.verzekering_datassur.AttestationsInsurance
uses gw.xml.date.XmlDateTime
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory

uses java.io.BufferedWriter
uses java.io.File
uses java.io.FileWriter
uses java.lang.Exception
uses java.lang.StringBuilder
uses java.text.SimpleDateFormat
uses java.util.Date
uses gw.blp.carattest.integration.utils.XmlDateTimeUtil
uses gw.blp.carattest.integration.common.CommonConstants_Ext
uses java.io.IOException

/**
 * Created with IntelliJ IDEA.
 * User: Abhijith.Rao1
 * Date: 6/11/16
 * Time: 3:10 PM
 * PcClaimAttestationToDattasurMapper
 *
 * This is the class where the claimAttestation entity which was populated during the claim import will be used to produce /generate the claim Attestion XML
 * in adherence with the contract of DATASSUR XSD, if all conditions satisfy then the XML will be generated and stored in the temp ftp location
 * For now the FTP location has been configured to point to the local dir path.
 *
 * Once the xml is generated , we mark the entity property :PolicyClaimAttestation.attestationGenerated
    as TRUE  which ensures no duplicate XML is ever generated for the impo
 *
 * TODO:  Document Generation is not part of the code now.
 */
class PolicyClaimAttestationToDatassurMapper {

  private static var LOGGER: Logger = LoggerFactory.getLogger(PolicyClaimAttestationToDatassurMapper)
  private static final var ftp_datassur_file_path = IntegrationPropertiesUtil.getProperty("ClaimImport_FTP_datassur_File_path")
  private var externalMatchingClaim: List<gw.blp.carattest.integration.xsd.claimmodel.anonymous.elements.ClaimSystem_Claims_Claim>
  private var period: PolicyPeriod
  public  var policyClaimAttestations: List<PolicyClaimAttestation_Ext>
  private var ATTESTATION_FILE_NAME = "attestationsinsurance_"
  private var ATTESTATION_FILE_DATE_FORMAT = "yyyyMMdd-HH-mm-ss-SSS"
  private var ATTESTATION_FILE_EXT = ".xml"

  construct(_period: PolicyPeriod) {
    period = _period
    policyClaimAttestations = findAllClaimAttestationsForPolicyToBeGenerated()
  }

  /**
   * function findAllClaimAttestationsForPolicyToBeGenerated()- Finds all the ClaimAttestation that matches the criteria which are pending for XML generation.
   *
   * @Returns  List<PolicyClaimAttestation_Ext>
   */
  function findAllClaimAttestationsForPolicyToBeGenerated(): List<PolicyClaimAttestation_Ext> {
    var matchedClaimAttestationsToBeGeneratedQuery = gw.api.database.Query.make(PolicyClaimAttestation_Ext)
    matchedClaimAttestationsToBeGeneratedQuery.and(\andCriteria -> {
      andCriteria.compareIgnoreCase(PolicyClaimAttestation_Ext#policyNumber, Relop.Equals, period.PolicyNumber)
      andCriteria.compare(PolicyClaimAttestation_Ext#attestationGenerated, Relop.Equals, false)
      andCriteria.compare(PolicyClaimAttestation_Ext#CreateTime, Relop.GreaterThanOrEquals, Date.CurrentDate.trimToMidnight())
    })

    var matchedClaimAttestationsToBeGenerated = matchedClaimAttestationsToBeGeneratedQuery.select()
    if (matchedClaimAttestationsToBeGenerated != null && matchedClaimAttestationsToBeGenerated.Count > 0){
      return matchedClaimAttestationsToBeGenerated.toList()
    }
    return null
  }

  /**
   * function checkConditions()- Checks whether the path where XML is generated is available
   *
   * @Returns  boolean
   */
  function checkConditions(): boolean {
    if (!new File(ftp_datassur_file_path).exists()) {
      LOGGER.error("Car@ttest attestation directory not found ${ftp_datassur_file_path}")
      throw new Exception("Incorrect Car@ttest attestation path: ${ftp_datassur_file_path}")
    }
    return true
  }

  /**
   * function createDatassurAttestation()- The actual claim attestation xml is generated from the entities
   *
   *
   */
  function createDatassurAttestation() {
    LOGGER.info("Entering createDatassurAttestation() Method : ")
    if (policyClaimAttestations != null && policyClaimAttestations.size() > 0) {
      gw.transaction.Transaction.runWithNewBundle(\bundle -> {
        var rootAttestationsDatassur = new AttestationsInsurance()
        var attestationCount = 0
        var nextInt = SequenceUtil.next(CommonConstants_Ext.CLAIM_ATTESTATION_UNIQUE_START_RANGE,CommonConstants_Ext.CLAIM_ATTESTATION_CONSTANT )

        rootAttestationsDatassur.GroupHeader.AttestationType    = CommonConstants_Ext.CLAIM_ATTESTATION_TYPE
        rootAttestationsDatassur.GroupHeader.User               = CommonConstants_Ext.CLAIM_ATTESTATION_EMAIL_ID
        //todo: get the user details
        rootAttestationsDatassur.GroupHeader.UniqueId           = nextInt
        rootAttestationsDatassur.GroupHeader.Nbb                = ScriptParameters.CarattestAttestationCompanyNBBCode
        rootAttestationsDatassur.GroupHeader.Timestamp          = new XmlDateTime()
        rootAttestationsDatassur.GroupHeader.TargetEnvironment  = TEST
        rootAttestationsDatassur.GroupHeader.NumberOfCases      = attestationCount

        var rootAttestations      = new gw.blp.carattest.integration.xsd.verzekering_datassur.anonymous.elements.AttestationsInsurance_Attestations()
        var rootAttestationsList  = new List<gw.blp.carattest.integration.xsd.verzekering_datassur.anonymous.elements.AttestationsInsurance_Attestations_Attestation>()

        policyClaimAttestations.each(\policyClaimAttestation -> {
            bundle.add(policyClaimAttestation)

            policyClaimAttestation.ClaimAttestations.each(\claimAttestation -> {

              var rootAttestation = new gw.blp.carattest.integration.xsd.verzekering_datassur.anonymous.elements.AttestationsInsurance_Attestations_Attestation()
              var attestation_AttestationInformation = new gw.blp.carattest.integration.xsd.verzekering_datassur.anonymous.elements.AttestationsInsurance_Attestations_Attestation_AttestationInformation()
                   //todo- dont create object of helper util
              attestation_AttestationInformation.ContrEndingDate = XmlDateTimeUtil.convertToXmlDate(claimAttestation.AttestationInformation.ContrEndingDate)
              attestation_AttestationInformation.ContrExpiryDate = XmlDateTimeUtil.convertToXmlMonthDay(claimAttestation.AttestationInformation.ContrExpiryDate)
              attestation_AttestationInformation.ContrStartingDate = XmlDateTimeUtil.convertToXmlDate(claimAttestation.AttestationInformation.ContrStartingDate)
              attestation_AttestationInformation.PolicyNumber   = claimAttestation.AttestationInformation.PolicyNumber
              attestation_AttestationInformation.PlateNumber    = claimAttestation.AttestationInformation.PlateNumber
              attestation_AttestationInformation.CreationDate   = XmlDateTimeUtil.convertToXmlDate(Date.CurrentDate.trimToMidnight())
              //attestation_AttestationInformation.CreationDate   = XmlDateTimeUtil.convertToXmlDate(claimAttestation.AttestationInformation.CreationDate)
              attestation_AttestationInformation.VehicleUse     = claimAttestation.AttestationInformation.VehicleUse
              attestation_AttestationInformation.VehicleType    = claimAttestation.AttestationInformation.VehicleType

              rootAttestation.AttestationInformation = attestation_AttestationInformation

              var attestation_InsuranceTakerInformation = new gw.blp.carattest.integration.xsd.verzekering_datassur.anonymous.elements.AttestationsInsurance_Attestations_Attestation_InsuranceTakerInformation()

              attestation_InsuranceTakerInformation.Gender    = claimAttestation.InsuranceTaker.Gender
              attestation_InsuranceTakerInformation.Language  = claimAttestation.InsuranceTaker.Language
              attestation_InsuranceTakerInformation.InsuranceTaker.BirthDate  = XmlDateTimeUtil.convertToXmlDate(claimAttestation.InsuranceTaker.BirthDate)
              attestation_InsuranceTakerInformation.InsuranceTaker.FirstName  = claimAttestation.InsuranceTaker.FirstName1
              attestation_InsuranceTakerInformation.InsuranceTaker.LastName   = claimAttestation.InsuranceTaker.LastName
              attestation_InsuranceTakerInformation.AddressInformation.BoxNumber    = claimAttestation.InsuranceTaker.BoxNumber
              attestation_InsuranceTakerInformation.AddressInformation.CountryCode  = claimAttestation.InsuranceTaker.CountryCode
              attestation_InsuranceTakerInformation.AddressInformation.City   = claimAttestation.InsuranceTaker.City
              attestation_InsuranceTakerInformation.AddressInformation.Street = claimAttestation.InsuranceTaker.Street
              attestation_InsuranceTakerInformation.AddressInformation.PostCode     = claimAttestation.InsuranceTaker.ZIPCode

              rootAttestation.InsuranceTakerInformation = attestation_InsuranceTakerInformation

              var outDisasterHistories = new gw.blp.carattest.integration.xsd.verzekering_datassur.anonymous.elements.AttestationsInsurance_Attestations_Attestation_DisasterHistories()
              var outDisasterHistoryList = new List<gw.blp.carattest.integration.xsd.verzekering_datassur.anonymous.elements.AttestationsInsurance_Attestations_Attestation_DisasterHistories_DisasterHistory>()

              claimAttestation.DisasterHistory.each(\history -> {

                var outDisasterHistory = new gw.blp.carattest.integration.xsd.verzekering_datassur.anonymous.elements.AttestationsInsurance_Attestations_Attestation_DisasterHistories_DisasterHistory()
                outDisasterHistory.DisasterDate = XmlDateTimeUtil.convertToXmlDate(history.DisasterDate)
                outDisasterHistory.DisasterType = history.DisasterType
                outDisasterHistory.Driver.BirthDate = XmlDateTimeUtil.convertToXmlDate(history.DriverBirthDate)
                outDisasterHistory.Driver.FirstName = history.DriverFirstName
                outDisasterHistory.Driver.LastName  = history.DriverLastName
                outDisasterHistory.AmountPaidArt29b = history.AmountPaidArt29b
                outDisasterHistory.AmountOfCompensation = history.AmountOfCompensation
                outDisasterHistory.Closed           = history.Closed
                outDisasterHistory.DriverResponsability = history.DriverResponsibility

                outDisasterHistoryList.add(outDisasterHistory)
              })
              if (outDisasterHistoryList.size() > 0){
                outDisasterHistories.DisasterHistory = outDisasterHistoryList
                //Only assign the list if we disaster history...
                rootAttestation.DisasterHistories = outDisasterHistories
              }
              rootAttestationsList.add(rootAttestation)
            })
            //end of one claimAttestation for policy ,Mark the record as generated for future reference which ensurers that duplicate xml are not generated
            policyClaimAttestation.attestationGenerated = true

        })
        rootAttestationsDatassur.GroupHeader.NumberOfCases = rootAttestationsList.Count
        rootAttestations.Attestation          = rootAttestationsList
        rootAttestationsDatassur.Attestations = rootAttestations
        LOGGER.info("Generated DatassurXML  : ${rootAttestationsDatassur.asUTFString()}")

        var fileGenerationStatus = generateAndSendXMLViaFTP(rootAttestationsDatassur.asUTFString())
          if(!fileGenerationStatus){
            throw new Exception("Exception while Generating XML hence not comitting the status")
          }
      }, PLConfigParameters.UnrestrictedUserName.Value)    //using the standard GW user for committing into DB
      LOGGER.info("Generated DatassurXML And Stored in FTP...")
    } else {
      LOGGER.info("Dint not generate DatassurXML as no claimAttestation to create...")
    }
    LOGGER.info("Entering createDatassurAttestation() Method : ")
  }

  /**
   * function generateAndSendXMLViaFTP()- Generates
   * @Param attestationXML xml string
   */
  function generateAndSendXMLViaFTP(attestationXML: String): boolean {
    LOGGER.info("Entered Method :generateAndSendXMLViaFTP()")
    var status = false
    var bufferedWriter: BufferedWriter = null
    try {

      if (checkConditions()) {

        var xmlFile = new File(AttestationPath)
        if (!xmlFile.exists()) {
          xmlFile.createNewFile()
        }
        var xmlFileWriter = new FileWriter(xmlFile.getAbsoluteFile())
        bufferedWriter = new BufferedWriter(xmlFileWriter)
        bufferedWriter.write(attestationXML)
        status = true
      } else {
        LOGGER.error("The FTP Location for datassur xml not found..")
        status = false
      }
    }
    catch (ioe: IOException) {
      LOGGER.error("IOException in generateAndSendXMLViaFTP: ${ioe.Message}")
      status = false
    }
    finally
    {
      try {
        if (bufferedWriter != null)
          bufferedWriter.close()
      } catch (ex: Exception) {
        LOGGER.error("Error in closing the BufferedWriter: ${ex.Message}")
        status = false
      }
     status = true
    }

    LOGGER.info("Leaving Method :generateAndSendXMLViaFTP()")
    return status
  }

  /*
 * Returns full path for Car@ttest Attestation
 * */

  private property get AttestationPath(): String {
    var _path = new StringBuilder(ftp_datassur_file_path).append(File.separator)
    _path.append(ATTESTATION_FILE_NAME)
        .append(new SimpleDateFormat(ATTESTATION_FILE_DATE_FORMAT).format(new Date()))
        .append(ATTESTATION_FILE_EXT)

    return _path.toString()
  }
}