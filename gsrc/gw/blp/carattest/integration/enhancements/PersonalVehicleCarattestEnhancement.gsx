package gw.blp.carattest.integration.enhancements

uses gw.blp.carattest.CarAttestVehicleUse

/**
 * PersonalVehicleCarattestEnhancement
 * User: Krzysztof Kornakiewicz krzysztof.kornakiewicz@xe05.ey.com
 * Date: 03.06.16
 * Time: 14:34
 */
enhancement PersonalVehicleCarattestEnhancement: entity.PersonalVehicle {

    /*VehicleUse() gets the corresponding values for CarAttest Equivalent Vehicle Use codes*/
  property get VehicleUse(): CarAttestVehicleUse {
    switch (this.PrimaryUse) {
      case(typekey.VehiclePrimaryUse.TC_BUSINESS):
          return CarAttestVehicleUse.Business
      case(typekey.VehiclePrimaryUse.TC_PLEASURE):
          return  CarAttestVehicleUse.Private
      case(typekey.VehiclePrimaryUse.TC_COMMUTING):
          return CarAttestVehicleUse.Business
      case(typekey.VehiclePrimaryUse.TC_MIXED):
          return CarAttestVehicleUse.Combined
      // Below is temporary - TODO: making PrimaryUse not nullok
      case(null) :
          return Combined
      default:
        throw "Invalid primary use" + this.PrimaryUse
        //TODO: Handling of other use. Ask David
    }
  }
}
