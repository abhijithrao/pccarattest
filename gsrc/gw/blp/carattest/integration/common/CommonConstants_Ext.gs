package gw.blp.carattest.integration.common

/**
 * CommonConstants_Ext interface defines constants required for CarAttest Accelerator
 * Author: Abhijith Rao
 */
uses java.lang.*
uses java.text.SimpleDateFormat
uses gw.blp.carattest.integration.utils.IntegrationPropertiesUtil

public interface CommonConstants_Ext {

   var INFLIGHT : String = "inflight"
   var DELIVERED : String = "DELIVERED"
   var FTP_PROCESSED : String = "FTP_PROCESSED"
   var FTP_FAILED : String = "FTP_FAILED"
   var SUCCESS : String = "SUCCESSS"
   var FAILED : String = "FAILED"
   var ARCHIVED : String = "ARCHIVED"
   var ARCHIEVE_FILE_SEPERATOR : String = "\\"
   var ARCHIEVE_FILE_DOUBLESEPERATOR : String = "\\\\"
   var ARCHIEVE_FOLDERNAME : String = "archive"
   var METHOD_ENTER : String = "Entering Method: "
   var METHOD_EXIT : String = "Exiting Method: "
   var VEHICLE_BODY_TYPECODE : String = "VEHICLE_BODY_TYPECODE"
   var VEHICLE_USE : String = "VEHICLE_USE"
   var VEHICLE_EFFECTIVE_DATE : String = "VEHICLE_EFFECTIVE_DATE"
   var VEHICLE_EXPIRATION_DATE : String = "VEHICLE_EXPIRATION_DATE"
   var CLAIM_ATTESTATION_CONSTANT : String = "ClaimAttestation"
   var CLAIM_ATTESTATION_TYPE : String = "Att"
   var CLAIM_ATTESTATION_EMAIL_ID : String = "olga.smirnova@tatv.be"
   var CLAIM_ATTESTATION_UNIQUE_START_RANGE : int = 1

   var DATE_TIME_FORMAT : SimpleDateFormat = new SimpleDateFormat("yyyyMMdd_HHmm")
   var DAYS_TO_EXPIRE : int = 15
   var NUMBER_OF_YEARS_FROM_TODAY : int = -5

}
