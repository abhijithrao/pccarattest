package gw.blp.carattest.integration.batch

uses gw.blp.carattest.integration.utils.CarAttestUtil
uses gw.blp.carattest.integration.utils.IntegrationPropertiesUtil
uses gw.processes.BatchProcessBase
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory

uses java.lang.Exception
uses java.io.File
uses gw.blp.carattest.integration.common.CommonConstants_Ext

/**
 *
 *
 * CreateClaimAttestationBatch_Ext collates all the policies that are about to expire and creates the Datassur claim attestation xml which will be sent to Datassur
 * Author: Abhijith
 *
 *   Batch Process Method for CreateClaimAttestationBatch
 *   1. Imports an xml file from FTP location , mimicking the process of hitting a 3rd party system and retrieving the claim information.
 *
 *   2. Collates all the Policies that are due to expire exactly equivalent to 15 days
 *
 *   3. For such above policies, searches a matching claim with in the range of 5 years in the imported file, and
 *   then populates/creates a new PolicyClaimAttestation entity per policy basis, there are boolean flags such docGenerated, attestationGenerated which is defaulted false during
 *
 *   4. after creation and populating the data in to the entities, the Datassur Claim Attestation XML is created as per the XSD Contract And saves in a FTP location/ For poc it is in local directory path.

 *
 */
class CreateClaimAttestationBatch_Ext extends BatchProcessBase {

  private static final var LOGGER: Logger = LoggerFactory.getLogger(CreateClaimAttestationBatch_Ext)
  private static final var file_name = IntegrationPropertiesUtil.getProperty("ClaimImport_file_name")
  private static final var source_File_path = IntegrationPropertiesUtil.getProperty("ClaimImport_source_File_path")
  private static final var targetFile = IntegrationPropertiesUtil.getProperty("ClaimImport_targetFile")
  private static final var client_separator = IntegrationPropertiesUtil.getProperty("ClaimImport_client_separator")
  private static final var daily_folder_pattern = IntegrationPropertiesUtil.getProperty("ClaimImport_file_daily_folder_pattern")
  private static final var archive_pass_file_path = IntegrationPropertiesUtil.getProperty("ClaimImport_archive_pass_file_path")
  private static final var archive_fail_file_path = IntegrationPropertiesUtil.getProperty("ClaimImport_archive_fail_file_path")
  private static final var ftp_archive_pass_file_path = IntegrationPropertiesUtil.getProperty("ClaimImport_source_FTPArchive_File_path")
  private static final var ftp_archive_fail_file_path = IntegrationPropertiesUtil.getProperty("ClaimImport_source_FTPFailed_File_path")
  private static final var date_time_format = IntegrationPropertiesUtil.getProperty("ClaimImport_date_time_format")
  var externalImportedClaims: List<gw.blp.carattest.integration.xsd.claimmodel.anonymous.elements.ClaimSystem_Claims_Claim>
  private var policiesToExpireAndCancelledYesterdayList : List<PolicyPeriod>

  var carAttestationUtil = new CarAttestUtil()
  construct() {
    super(BatchProcessType.TC_CARATTESTATION_EXT)
  }

  override function requestTermination() :boolean {
    super.requestTermination() // set the TerminationRequested flag
    return true // return true to signal that we will attempt to terminate in our doWork method
  }
  override function checkInitialConditions(): boolean {
    policiesToExpireAndCancelledYesterdayList = carAttestationUtil.getPoliciesExpiringInFifteenDaysAndCancelledYesterday(CommonConstants_Ext.DAYS_TO_EXPIRE)
    return
        (
            checkConditionIfClaimImportFileExists(file_name, source_File_path)  &&      //if ClaimFile is available for import
                policiesToExpireAndCancelledYesterdayList != null &&       //Check if there are any policies Found , matching our criteria,
                policiesToExpireAndCancelledYesterdayList.Count > 0
                //if there is atleast one policy which is due to expire in 15 days OR
                // any policy which was cancelled yesterday or
                // any Past Dated cancellation made yesterday

        )
  }

  override function doWork() {
    if (TerminateRequested){
      return
    }
    LOGGER.info("CreateClaimAttestationBatchBatch :doWork : Begins ")
    var status = 0
    super.setOperationsExpected(policiesToExpireAndCancelledYesterdayList.Count)
    //Set Number of expected operations , number of policyPeriod found for processing is Number of operations
    try {
      //import only once
      externalImportedClaims = carAttestationUtil.processSourceFileAndGetClaims(file_name, source_File_path)
      LOGGER.info("Total Number Of Policies found that are about to expire : ${policiesToExpireAndCancelledYesterdayList.Count}")
      if (externalImportedClaims == null && !externalImportedClaims.HasElements) {

        LOGGER.info("Batch : No Claim Import Records Found during import process and Hence Skipping the procces itself..!")
      }
      else {
        LOGGER.info("Batch : Claim Import Records Found : Processing Starts..!")
        policiesToExpireAndCancelledYesterdayList?.each(\filteredPolicy -> {
          //If a user clicks the Stop button on the Batch Process Info page, this requests the batch process to terminate
          if (TerminateRequested) {
            return
          }

          var policyPeriod = filteredPolicy.getSlice(filteredPolicy.EditEffectiveDate)

          if (externalImportedClaims.Count > 0 && !carAttestationUtil.validateIfClaimAttestationGenerated(policyPeriod)) {
            var matchedClaims = carAttestationUtil.findClaimsAndCreateAttestations(policyPeriod, externalImportedClaims)
            if (matchedClaims != null) {

              carAttestationUtil.createCarAttestation(policyPeriod, matchedClaims)
              //this is creation of entities
              var datassurXMLMapAndCreate = new gw.blp.carattest.integration.mappers.PolicyClaimAttestationToDatassurMapper(policyPeriod)
              //creation of Entity to Datassur XML

              datassurXMLMapAndCreate.createDatassurAttestation()
            }
            else {
              LOGGER.info("CreateClaimAttestationBatch :doWork : No Matching Claim Found for Policy . Going to next record.. ")
            }
          } else {
            LOGGER.info("CreateClaimAttestationBatch :doWork : No Claims Imported or ClaimAttestation already generated todate for the given policy... ")
          }
          incrementOperationsCompleted()
          //Irrespective of Xml generated we mark operation as complete, as there may not be claims for every policy for which
          //claim attestation may not be generated
        })
      }
    } catch (e: Exception) {
      LOGGER.error(" Exception in CreateClaimAttestationBatch :doWork()  method.. : ${e.Message}")
      handleImportException(e, "Exception in CreateClaimAttestationBatch :doWork()")
    }
    LOGGER.info("CreateClaimAttestationBatch :doWork() : Ends ")
  }

  /**
   * Increments Operations failed and log exception cause and stack trace
   *
   */
  @Param("ex", "The data type is: Exception")
  @Param("message", "The data type is: String")
  private function handleImportException(ex: Exception, message: String) {
    LOGGER.error(message)
    LOGGER.error(ex.StackTraceAsString)
    incrementOperationsFailed()
    OperationsFailedReasons.add(message)
  }

  /**
   * checkConditionIfClaimImportFileExists()
   * Checks whether the claim import file exists or noy
   *
   */
  @Param("fileName", "The file name of the claim file that needs to be processed")
  @Param("sourceFilePath", "The full path of the file")
  private function checkConditionIfClaimImportFileExists(fileName: String, sourceFilePath: String) : boolean{

    LOGGER.info("ClaimImport : processSourceFileAndGetClaims : Begins ")
    var status = false
    var file : File = null
    try {
      LOGGER.info("ClaimImport : fileName : ${fileName}, sourceFilePath :${sourceFilePath}")
      var sourcePath ="${sourceFilePath}${fileName}"

      LOGGER.debug("ClaimImport : sourcePath : ${sourcePath}")
      LOGGER.debug("ClaimImport : env :${java.lang.System.getProperty("gw.pc.env")}")

      file = new File(sourcePath)
      if (file.canRead()) {
        status = true
      }
    } catch (ex: Exception) {
      handleImportException(ex, "Exception during file read and import : ")
      status = false
    }
    return status
  }




}

