package gw.blp.carattest
/**
 * Created on behalf EY GTH Poland Sp.z.o.o.
 * User: Krzysztof Kornakiewicz - krzysztof.kornakiewicz@xe05.ey.com
 * Date: 03.06.16
 */
enum CarAttestVehicleUse {
  Business("2"), Private("1"), Combined("3")

  var carAttestCode : String as CarAttestCode

  private construct(code : String) {
    this.carAttestCode = code
  }
}