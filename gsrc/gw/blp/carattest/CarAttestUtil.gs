package gw.blp.carattest

uses java.util.Date
uses java.util.ArrayList
uses java.util.LinkedList
uses gw.api.util.DateUtil

/**
 * Created on behalf EY GTH Poland Sp.z.o.o.
 * User: Krzysztof Kornakiewicz - krzysztof.kornakiewicz@xe05.ey.com
 * Date: 08.06.16
 */
class CarAttestUtil {
  static function createCarAttestation(period: PolicyPeriod): PolicyClaimAttestation_Ext {
    var liabilityCoverage = period.PersonalAutoLine.CoveragesFromCoverable.whereTypeIs(PALiabilityCov)?.single()
    print(liabilityCoverage.CoverageCategory)
    var account = period.Policy.Account

    // TODO: Provide best solution to handle
    if (liabilityCoverage == null){
      throw "No TPL coverage or found on this PolicyPeriod!"
    }
    if (account typeis Person){
      throw "PolicyHolder is not a Person type."
    }
    var person = (account.AccountHolderContact as Person)
    var insuranceTaker = new InsuranceTaker_Ext()
    insuranceTaker.language = mapLanguageCode(account.PrimaryLanguage)
    insuranceTaker.gender = mapGenderCode(person.Gender)
    insuranceTaker.lastName = person.LastName
    insuranceTaker.firstName1 = person.FirstName
    insuranceTaker.birthDate = person.DateOfBirth
    //TODO: Parse to house number and box code
    insuranceTaker.street = person.PrimaryAddress.AddressLine1
    insuranceTaker.street = person.PrimaryAddress.CityStateZip
    // TODO: verifiy if that's correct field
    insuranceTaker.city = person.CityDenorm
    insuranceTaker.countryCode = person.Country.Code
    // TODO: firstname2, firstname3 and birthplace

    var policyClaimAttestation = new PolicyClaimAttestation_Ext()
    for (vehicle in period.PersonalAutoLine.Vehicles) {
      var attestationInformation = new AttestationInformation_Ext()
      attestationInformation.policyNumber = period.PolicyNumber
      attestationInformation.plateNumber = vehicle.LicensePlate
      attestationInformation.contrStartingDate = liabilityCoverage.EffectiveDate
      attestationInformation.contrExpiryDate = liabilityCoverage.ExpirationDate
      attestationInformation.contrEndingDate = period.CancellationDate
      attestationInformation.vehicleType = vehicle.BodyType.Code
      attestationInformation.vehicleUse = vehicle.VehicleUse.CarAttestCode
      attestationInformation.creationDate = DateUtil.currentDate()
      var claimAttestation = new ClaimAttestation_Ext()
      claimAttestation.attestationInformation = attestationInformation
      // TODO: Change to scriptparameter
      claimAttestation.companyNBBCode = "00196"
      claimAttestation.insuranceTaker = insuranceTaker
      policyClaimAttestation.addToClaimAttestations(claimAttestation)
    }

    return policyClaimAttestation
  }

  static function mapGenderCode(gender: GenderType): String {
    switch (gender) {
      case(GenderType.TC_F): return "1"
      case(GenderType.TC_M): return "2"
        default: return "3"
    }
  }

  static function mapLanguageCode(language: LanguageType): String {
    switch (language) {
      case(LanguageType.TC_NL): return "1"
      case(LanguageType.TC_FR): return "2"
      case(LanguageType.TC_DE): return "3"
      case(LanguageType.TC_EN_US): return "4"
        default: return "0"
    }
  }
}